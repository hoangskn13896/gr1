const am_dau = ["b", "c", "ch", "d", "g", "gh", "h", "k", "kh", "l", "m", "n", "ng",
  "ngh", "nh", "p", "ph", "q", "r", "s", "t", "th", "tr", "v", "x"];

const am_giua = ["a", "e", "i", "o", "u", "y", "ai", "ao", "au", "ay", "eo", "ia","ic",
  "iu", "oa", "an", "oe", "oi", "oo", "on", "ua", "un", "en", "ui", "uy", "oai", "oay",
  "oeo", "uao", "uya", "uyu"];

const am_cuoi = ["c", "ch", "m", "n", "ng", "nh", "p", "t"];

module.exports = function checkChinhTa(word) {
    word = change(word);
    var am_tiet = word.split("");
    //check am dau
    var tmp_am_dau = [];
    am_dau.forEach(function (ad) {
      if (am_tiet[0] === ad) tmp_am_dau.push(ad);
      if (am_tiet.length > 2) {
        if (am_tiet[0] + am_tiet[1] === ad) {
          tmp_am_dau.push(ad);
        }
      }
      if (am_tiet.length > 3) {
        if (am_tiet[0] + am_tiet[1] + am_tiet[2] === ad) {
          tmp_am_dau.push(ad);
        }
      }
    });
    //check am cuoi
    var tmp_am_cuoi = [];
    am_cuoi.forEach(function (ac) {
      if (am_tiet[am_tiet.length - 1] === ac) tmp_am_cuoi.push(ac);
      if (am_tiet[am_tiet.length - 2] + am_tiet[am_tiet.length - 1] === ac) tmp_am_cuoi.push(ac);
    }
    )
    //check am giua
    var tmp_am_giua = am_tiet.slice(tmp_am_dau.length, am_tiet.length - tmp_am_cuoi.length).join('');
    console.log("tach am: "+tmp_am_dau+"+"+tmp_am_giua+"+"+tmp_am_cuoi)
    var kq = false;
    am_giua.forEach(function (ag) {
      if (ag === tmp_am_giua) kq = true;
    })
    return kq;
  }

function change(alias) {
    var str = alias;
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/ + /g, " ");
    return str;
  }  