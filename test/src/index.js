import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import check from './checkWord';

const checkChinhTa = require('./checkChinhTa');

const viet_tat = [{ tu: "ktx", nghia: "ký túc xá" }, { tu: "ah", nghia: "à" }, { tu: "ct", nghia: "công ty" },
{ tu: "bk", nghia: "bách khoa" }, { tu: "t", nghia: "tớ" }, { tu: "m", nghia: "mày" }]

const common = [{ tu: "t", nghia: "tôi" }, { tu: "b", nghia: "bạn" }]

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputString: '',
      outputArr: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ inputString: event.target.value });
  }

  handleSubmit(event) {
    var arr = check.checkWord(this.state.inputString);
    for (var i = 0; i < arr.length; i++) {
      var word = arr[i].w;
      word = word.toLowerCase();
      var nghia;
      viet_tat.forEach(ob => {
        if (ob.tu === word) nghia = ob.nghia;
      })
      if (typeof (nghia) === "undefined" || nghia === null) {
        common.forEach(ob => {
          if (ob.tu === word) nghia = ob.nghia;
        })
      }
      if (typeof (nghia) !== "undefined" && nghia !== null) {
        arr[i].goc = arr[i].w;
        arr[i].w = nghia;
        arr[i].type = "vt"; 
        nghia = null;
      }
      if (!check.dau.includes(word)){
        if(checkChinhTa(word)) arr[i].type = "t";
        else if(arr[i].type !== "vt") arr[i].type = "f";
      }
      else arr[i].type="d";
    }
     this.setState({ outputArr: arr });
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Essay:
            <textarea value={this.state.inputString} onChange={this.handleChange} />
          </label>
          <input type="submit" value="Submit" />
        </form>
        <OutputDialog outputArr={this.state.outputArr}/>
      </div>
    );
  }
}

class OutputDialog extends React.Component {

	constructor(props) {
    super(props);
    this.state = {
      visible: false,
      value: []
  };
  }

  popup(par) {
    this.setState({
      visible: true,
      value: par
    })
  }

  render(){
    const popup = (this.state.visible ? <Popup value={this.state.value} /> : null);
    var pars = this.props.outputArr.map(par => {
      if(par.type==="vt") return <span className="vietTat" key={par.i} onClick={this.popup.bind(this,par)}> {par.w}</span>
      if(par.type==="f") return <span className="saiChinhTa" key={par.i} onClick={this.popup.bind(this,par)}> {par.w}</span>
      if(par.type==="d") return <span key={par.i}>{par.w}</span>
      if(par.type==="t") return <span key={par.i}> {par.w}</span>
    });
    
    return(
      <div>
        {popup}
        {pars}
      </div>
    )
  }
}

class Popup extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {value} = this.props;
    return (
      <div className="popup">
        <p>{value.w}</p>
        <p>{value.goc}</p>
      </div>
    )
  }
}

class AddDialog extends React.Component{

}

class PatchDialog extends React.Component{
  
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
