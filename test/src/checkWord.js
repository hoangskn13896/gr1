const dau = ["\n", ".", ",", "!", ":", ";", "?", "\"", "@", "_", "%", "^", "*", "(", ")", "+", "=", "<", ">", "/", "\\", "'",
  "|", "&", "#", "[", "]", "~", "$", "`", "{", "}"];

function checkWord(words) {
  var arr = words.split(' ');
  arr = checkDau(arr);
  arr = arr.map((i, index) => {
    var out = { w: i, i: index, type: "" };
    return out;
  })
  return arr;
}

function checkDau(arr) {
  var outArr = [];
  for (let i = 0; i < arr.length; i++) {
    var element = arr[i];
    for (let j = 0; j < dau.length; j++) {
      var d = dau[j];
      var tmp = element.split(d);
      if (tmp.length > 1) {
        for (let x = 0; x < tmp.length; x++) {
          var ite = tmp[x];
          if (ite === "") { tmp[x] = d; }
          if (ite !== "" && tmp[x + 1] !== "") {
            tmp.splice(1, 0, d);
            break;
          }
        }
        break;
      }
    }
    outArr = outArr.concat(tmp);
  }
  return outArr;
}

export default { dau,checkWord };